package assignmentTwo

import java.awt.Dimension
import java.awt.Graphics2D
import java.rmi.Naming
import java.rmi.Remote
import java.rmi.server.UnicastRemoteObject

import scala.swing.Action
import scala.swing.BorderPanel
import scala.swing.ListView
import scala.swing.MainFrame
import scala.swing.Panel
import scala.swing.ScrollPane
import scala.swing.TextArea
import scala.swing.TextField
import scala.swing.event.Key
import scala.swing.event.KeyPressed
import scala.swing.event.KeyReleased

/*RMI
 * - allows us to deviate from using client to server relationship to send messages
 * - we can just use peer to peer connections
 * - clients will have remote references to other clients 
*/
@remote trait RemoteClient extends Remote { //step 1
  def updateLevel(pl: PassableLevel)
  def name: String //get name of client
  def clientUpdate(clients: Seq[Player]): Unit //update list of clients logged in 
}

@remote object ClientMain extends UnicastRemoteObject with RemoteClient {
  var myName = ""
  var clients = Seq[Player]()
  private var message = ""
  def name: String = myName

  def updateLevel(pl: PassableLevel): Unit = {
    level = pl
    if (panel != null) panel.repaint
  }

  def clientUpdate(cls: Seq[Player]) {
    clients = cls
    if (userList != null) userList.listData = cls
  }

  private val userList = new ListView(clients)
  private val chatField = new TextField("")
  val chatText = new TextArea
  val mazeLength = 700
  val mazeWidth = 700

  val server = Naming.lookup("rmi://localhost/RMIServer") match { //step 4
    case s: RemoteServer => s
    case other           => throw new RuntimeException("Bound object is wrong type. " + other)
  }

  val playerNumber = server.connect(this) //step 4

  private var level: PassableLevel = null
  private val renderer = new Renderer
  val panel = new BorderPanel {
    layout += new BorderPanel {
      val chatField = new TextField("")
      val scrollChat = new ScrollPane(chatText)
      layout += chatField -> BorderPanel.Position.South
      layout += scrollChat -> BorderPanel.Position.Center
    } -> BorderPanel.Position.South
    layout += new BorderPanel {
      val activePlayers = new ScrollPane(userList)
      layout += activePlayers -> BorderPanel.Position.West
      layout += new Panel {
        override def paint(g: Graphics2D): Unit = {
          if (level != null) {
            renderer.render(g, level, mazeWidth, mazeLength)
          }
        }
        preferredSize = new Dimension(mazeLength, mazeWidth)
      } -> BorderPanel.Position.Center
    } -> BorderPanel.Position.Center
    listenTo(keys)
    reactions += {
      case e: KeyPressed =>
        println("Client key pressed")
        if (e.key == Key.Up) playerNumber.upPressed
        else if (e.key == Key.Down) playerNumber.downPressed
        else if (e.key == Key.Left) playerNumber.leftPressed
        else if (e.key == Key.Right) playerNumber.rightPressed
      case kr: KeyReleased =>
        if (kr.key == Key.Up) playerNumber.upReleased
        else if (kr.key == Key.Down) playerNumber.downReleased
        else if (kr.key == Key.Left) playerNumber.leftReleased
        else if (kr.key == Key.Right) playerNumber.rightReleased
    }
  }

  val frame = new MainFrame {
    title = "Pacman"
    contents = panel
    centerOnScreen
  }

  def main(args: Array[String]) {
    frame.open
    panel.requestFocus
  }
  //make enemy follow player
  //  enemies.move(player.posx,player.posy) 

  //end game
  /* if(player == player(0).killedBy(enemies)){
        println("Game Over")
        sys.exit(0)))) 
          
  } */
}