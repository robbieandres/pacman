package assignmentTwo

import java.awt.Color
import java.awt.Graphics2D
import java.awt.geom.Ellipse2D
import java.awt.geom.Rectangle2D

class Renderer {

  def render(g:Graphics2D, level:PassableLevel, width:Double, height:Double):Unit = {
    val boxWidth = width.toDouble/level.maze.length
    val boxHeight = height.toDouble/level.maze(0).length
    for(i <- level.maze.indices; j <- level.maze(i).indices) {
      level.maze(i)(j) match {
        case Wall => g.setPaint(Color.black) 
        case Floor => g.setPaint(Color.white) 
        case EnemyDoor => g.setPaint(Color.blue)
      }
      g.fill(new Rectangle2D.Double(j*boxWidth,i*boxHeight,boxWidth,boxHeight))
    }
    
    for(e <- level.characters) {
      e.etype match {
        case Entity.enemyValue => g.setPaint(Color.cyan)
        case Entity.playerValue => g.setPaint(Color.pink)
      }
      g.fill(new Ellipse2D.Double(e.x*boxWidth,e.y*boxHeight,boxWidth,boxHeight))
    }
    
 /*   for(e <- level.users) {
      g.setPaint(Color.yellow) 
      g.fill(new Ellipse2D.Double(e.x*boxWidth,e.y*boxHeight,boxWidth,boxHeight))
    }   */
 /*   for(e <- level.multiplePlayers) {
      g.setPaint(Color.yellow)
      g.fill(new Ellipse2D.Double(e.x*boxWidth,e.y*boxHeight,boxWidth,boxHeight))
    } */
  }
}