package assignmentTwo

import java.rmi.Naming
import java.rmi.Remote
import java.rmi.registry.LocateRegistry
import java.rmi.server.UnicastRemoteObject
import javax.swing.Timer
//import java.util.Timer
import scala.collection.mutable
import scala.swing._
//import java.util.TimerTask

@remote trait RemoteServer extends Remote { //step 1
  def connect(client: RemoteClient): RemotePlayer
}

object ServerMain extends UnicastRemoteObject with RemoteServer { //step 1

  val maze = Array(Array(-1, -1, -1, -1, 0, -1, -1, 2, -1, -1, 0, -1, -1, -1, -1),
    Array(-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1),
    Array(-1, 0, -1, -1, 0, -1, -1, 0, -1, -1, 0, -1, -1, 0, -1),
    Array(-1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1),
    Array(-1, 0, -1, -1, 0, 0, 0, -1, -1, -1, -1, 0, -1, -1, -1),
    Array(0, 0, 0, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    Array(-1, -1, 0, 0, 0, 0, 0, -1, -1, 0, -1, -1, 0, -1, -1),
    Array(2, 0, 0, -1, 0, -1, 0, 0, -1, 0, -1, 0, 0, 0, 2),
    Array(-1, 0, -1, -1, 0, -1, -1, 0, 0, 0, -1, 0, -1, 0, -1),
    Array(0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, -1, 0, 0),
    Array(-1, -1, -1, 0, -1, -1, -1, 0, 0, 0, -1, 0, -1, 0, -1),
    Array(-1, 0, 0, 0, 0, -1, 0, 0, -1, 0, -1, 0, 0, 0, -1),
    Array(-1, 0, -1, -1, 0, 0, 0, -1, -1, 0, -1, -1, -1, 0, -1),
    Array(-1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1),
    Array(-1, -1, -1, -1, 0, -1, -1, 2, -1, -1, 0, -1, -1, -1, -1))

  val enemy1 = new Enemy(7, 8)
  val enemy2 = new Enemy(6, 6)
  val entities = mutable.ListBuffer[Entity](enemy1)
  entities.foreach(_.level = level)

  private var players = List[Player]()
  val level = new Level(maze, entities, Nil)
  println(level.depthShortestPath(maze, 0, 0, 9, 9, 1))
  println(level.breadthFirstShortestPath(0, 0, 9, 9))
  val timer = new Timer(100, Swing.ActionListener { ae =>
    level.updateAll
    level.players.foreach(_.client.updateLevel(level.buildPassable))
  })

  def connect(client: RemoteClient): RemotePlayer = {
    val p = new Player(2, 1, client)
    players ::= p
    level.addingPlayer(p)
    p: RemotePlayer
  }

  /* def connect(client: RemoteClient): RemotePlayer = { //players join game
    players.synchronized {
      if (players.length < 2) {
        val p = new Player(if (players.isEmpty) 5 else 11,
          if (players.isEmpty) 1 else 3, client)
        players ::= p
        level.addingPlayer(p)
        if (players.length > 1) {
          timer.scheduleAtFixedRate(new TimerTask {
            def run {
              update2
            }
          }, 0, 100)
        }
        p: RemotePlayer
      } else null
    }
  } 
*/
  /* private def sendUpdate { //sends out a message to all the clients letting them know that the client list has been modified
    val deadClients = players.filter(c =>
      try {
        false
      } catch {
        case ex: RemoteException => true
      })
    players --= deadClients
    players.foreach(_.client.clientUpdate(players))
  }  */

  /* def disconnect(client:RemoteClient) = {
    clients -= client
    sendUpdate
  }
  
  def getClients:Seq[RemoteClient] = clients
  
  def publicMessage(client:RemoteClient, text:String) {
    history += client.name+" : "+text
    if(history.length>10) history.remove(0)
  } */

  def update2 {
    level.characters.foreach(_.update)
    players.foreach(_.client.updateLevel(level.buildPassable))
  }

  def main(args: Array[String]): Unit = {
    LocateRegistry.createRegistry(1099) //1099 default port number of RMI  & this is step 3
    Naming.rebind("RMIServer", this)
    timer.start
  //  if (players(0).killedBy(enemies) == true) {
  //    timer.stop()
  //    println("what?")
  //  }
  }

}