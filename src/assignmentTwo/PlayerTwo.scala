package assignmentTwo

import java.awt.Graphics2D
import java.awt.geom.Ellipse2D
import java.awt.Color

class PlayerTwo (initPosX:Double, initPosY:Double)  {

 private var posx = initPosX
 private var posy = initPosY
  
  def drawPlayer(g:Graphics2D) {
    g.setPaint(Color.yellow)
    g.fill(new Ellipse2D.Double (posx,posy,700/20,800/20))   // (posx,posy,widthP,lengthPlayer)
  }
 
  def up(): Unit =  {
    posy -= 50
  }
  
  def down(): Unit = {
    posy += 50
  }
  
  def left(): Unit =  {
    posx -= 800/15
  }
  
  def right(): Unit =  {
    posx += 800/15
  }
 
 
}