package assignmentTwo

import scala.collection.mutable

class Level(intMaze: Array[Array[Int]], private var chars: mutable.ListBuffer[Entity], private var play: List[RemotePlayer]) extends Serializable {
  val maze = intMaze.map(row => row.map(_ match {
    case 0  => Floor
    case -1 => Wall
    case 2  => EnemyDoor
  }))
  chars.foreach { e => e.level = this }

  def players = chars.collect { case p:Player => p}
  
  private val eList = mutable.ListBuffer[Enemy]()
  private val pList = mutable.ListBuffer[Player]()
  def filterEntity = {
    eList.clear()
    pList.clear()
    for (n <- chars) {
      n match {
        case enemy: Enemy   => eList += enemy
        case player: Player => pList += player
      }
    }
  }
  def elist = eList
  def plist = pList

  def depthShortestPath(maze: Array[Array[Int]], x: Int, y: Int, ex: Int, ey: Int, steps: Int): Int = {
    if (x == ex && y == ey) 0 //checks you arent at the end  
    else if (x < 0 || x >= maze.length || y < 0 || y >= maze(x).length || maze(x)(y) < 0) { //makes sure you arent out of area or hitting a wall
      1000000000 //not -1 or Int.MaxValue because maxint +1 is a negative value which will be min //not -1 or Int.MaxValue because maxint +1 is a negative value which will be min
    } else if (maze(x)(y) > 0 && maze(x)(y) <= steps) {
      1000000000 //if we breakout //if we breakout
    } else {
      maze(x)(y) = steps //have to drop breadcrumbs
      ClientMain.panel.repaint //drop a breadcrumb
      Thread.sleep(20)
      val ret = (depthShortestPath(maze, x + 1, y, ex, ey, steps + 1) min
        depthShortestPath(maze, x - 1, y, ex, ey, steps + 1) min
        depthShortestPath(maze, x, y + 1, ex, ey, steps + 1) min
        depthShortestPath(maze, x, y - 1, ex, ey, steps + 1)) + 1 //+1 because you have to take a step to get there
      ret
    }
  }

  def breadthFirstShortestPath(x: Int, y: Int, ex: Int, ey: Int): Int = {
    val queue = scala.collection.mutable.Queue[(Int, Int, Int)]()
    val visited = scala.collection.mutable.Set[(Int, Int)]()

    def happySpot(cx: Int, cy: Int): Boolean = {
      cx >= 0 && cy >= 0 && cx < maze.length && cy < maze(x).length && maze(cx)(cy).canPass &&
        !visited((cx, cy))
    }

    queue.enqueue((x, y, 0))
    visited.add((x, y))
    while (queue.nonEmpty) {
      val (cx, cy, steps) = queue.dequeue()
      if (cx == ex && cy == ey) return steps
      if (happySpot(cx + 1, cy)) { queue.enqueue((cx + 1, cy, steps + 1)); visited += ((cx + 1, cy)) }
      if (happySpot(cx - 1, cy)) { queue.enqueue((cx - 1, cy, steps + 1)); visited += ((cx - 1, cy)) }
      if (happySpot(cx, cy + 1)) { queue.enqueue((cx, cy + 1, steps + 1)); visited += ((cx, cy + 1)) }
      if (happySpot(cx, cy - 1)) { queue.enqueue((cx, cy - 1, steps + 1)); visited += ((cx, cy - 1)) }
    }
    100000000
  }

  def rPlayers = chars.collect {
    case p:Player =>p
  }
  
  def buildPassable: PassableLevel = {
    new PassableLevel(maze, chars.map(_.makePassable).toArray)
  }

  def updateAll: Unit = chars.foreach { _.update }

  def addingPlayer(p: Entity): Unit = {
    chars += p
    p.level = this
  }
  def addingEnemy(e: Enemy) = {
    chars == e
  }
  def characters = chars
  //  def users = play
}