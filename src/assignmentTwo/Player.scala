package assignmentTwo

import  scala.collection.mutable
import java.awt.Graphics2D
import java.awt.Color
import java.awt.geom.Ellipse2D
import java.rmi.server.UnicastRemoteObject
import java.rmi.Remote

@remote trait RemotePlayer extends Remote {
  def upPressed: Unit
  def downPressed: Unit
  def leftPressed: Unit
  def rightPressed: Unit
  def upReleased: Unit
  def downReleased: Unit
  def leftReleased: Unit
  def rightReleased: Unit
  def x: Double
  def y: Double
}

class Player(ex: Double, ey: Double, val client: RemoteClient) extends UnicastRemoteObject with Entity with RemotePlayer {
  private var mLevel: Level = null

  def px = ex
  def py = ey
  val boxW = 700 / 15
  val boxL = 700 / 15
  var x = ex
  var y = ey
  private var up = false
  private var down = false
  private var left = false
  private var right = false

 /* def level = mLevel
  def level_=(l: Level): Unit = mLevel = l */

  def drawPlayer(g: Graphics2D) {
    g.setPaint(Color.yellow)
    g.fill(new Ellipse2D.Double(x * boxW, y * boxL, 700 / 15, 700 / 15)) // (posx,posy,widthP,lengthPlayer)
  }

  def upPressed: Unit = {
    println("player up pressed " + this)
    up = true
  }
  def downPressed: Unit = down = true
  def leftPressed: Unit = left = true
  def rightPressed: Unit = right = true
  def upReleased: Unit = up = false
  def downReleased: Unit = down = false
  def leftReleased: Unit = left = false
  def rightReleased: Unit = right = false

  override def update(): Unit = {
    var nx = x
    var ny = y
    if (up) ny -= 1
    if (down) ny += 1
    if (left) nx -= 1
    if (right) nx += 1
    if (nx >= 0 && nx < level.maze.size && ny >= 0 && ny < level.maze(0).size && level.maze(ny.toInt)(nx.toInt).canPass) {
      x = nx
      y = ny
    }
  }

  def makePassable: PassableEntity = new PassableEntity(x, y, Entity.playerValue)

  def killedBy(enemies: mutable.ListBuffer[Enemy]): Boolean = {
    enemies.exists(_.hits(ex,ey))
  //  .hits(ex, ey) //enemy is in current position of player
  }

}