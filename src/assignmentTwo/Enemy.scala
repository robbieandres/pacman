package assignmentTwo

import java.awt.Color
import java.awt.Graphics2D
import java.awt.geom.Ellipse2D

class Enemy(initPosX: Double, initPosY: Double) extends Entity {
  protected var x = initPosX
  protected var y = initPosY

  val size = math.Pi * (800 / 30 * 800 / 30) //area of cirlce

  /*def move(endX:Int,endY:Int) { //make enemy chase player
      val dx = endX - initPosX
      val dy = endY - initPosY
      val len = math.sqrt(dx*dx+dy*dy)
      initPosX += dx/len
      initPosY += dy/len
  } */

  def hits(playerX: Double, playerY: Double): Boolean = { //support killedBy in player
    val dx = initPosX - playerX
    val dy = initPosX - playerY
    val dist = math.sqrt(dx * dx + dy * dy) //distance equation
    dist <= size //size = area of enemy & player   
  }

  //  def level = mLevel

  //  def level_= (l: Level): Unit = mLevel = l //what is this???

  override def update(): Unit = {
    //    val nx = x + util.Random.nextInt(3) - 1 //randomly move to a location
    //    val ny = y + util.Random.nextInt(3) - 1
    // if (nx >= 0 && nx < level.maze.size && ny >= 0 && ny < level.maze(0).size && level.maze(ny.toInt)(nx.toInt).canPass) {
    // level.filterEntity
    var highV = 1000000000.00
    var distP: Player = null
    for (i <- level.players) {
      val cX = i.x - x
      val cY = i.y - y
      val distance = math.sqrt(cX * cX + cY * cY)
      if (distance < highV) {
        highV = distance
        distP = i
      }
      println("sweet")
    }
    if (distP == null) return
    val a = level.breadthFirstShortestPath(x.toInt, (y - 1).toInt, distP.getx.toInt, distP.gety.toInt)
    println(a)
    val b = level.breadthFirstShortestPath(x.toInt, (y + 1).toInt, distP.getx.toInt, distP.gety.toInt)
    println(b)
    val c = level.breadthFirstShortestPath((x + 1).toInt, y.toInt, distP.getx.toInt, distP.gety.toInt)
    println(c)
    val d = level.breadthFirstShortestPath((x - 1).toInt, y.toInt, distP.getx.toInt, distP.gety.toInt)
    println(d)
    val shortestRoute = List(a, b, c, d).min
    if (shortestRoute < 1000000000) {
      var newx = x
      var newy = y
      if (shortestRoute == a) newy -= 1
      else if (shortestRoute == b) newy += 1
      else if (shortestRoute == c) newx += 1
      else if (shortestRoute == d) newx -= 1
      x = newx
      y = newy
      //get player current position than got +1 -1 -maze.length + maze.length to avoid diagonal

    }
    //    level.filterEntity
    //    var highV = 1000000000.00
    //    var distP: Player = null
    //    for (i <- level.plist) {
    //      val cX = i.x - x
    //      val cY = i.y - y
    //      val distance = math.sqrt(cX * cX + cY * cY)
    //      if (distance < highV) {
    //        highV = distance
    //        distP = i
    //      }
    //    }
  }
  //
  //    if (distP == null) return
  //    val a = level.breadthFirstShortestPath(x.toInt, (y - 1).toInt, distP.getx.toInt, distP.gety.toInt)
  //    val b = level.breadthFirstShortestPath(x.toInt, (y + 1).toInt, distP.getx.toInt, distP.gety.toInt)
  //    val c = level.breadthFirstShortestPath((x + 1).toInt, y.toInt, distP.getx.toInt, distP.gety.toInt)
  //    val d = level.breadthFirstShortestPath((x - 1).toInt, y.toInt, distP.getx.toInt, distP.gety.toInt)
  //    val shortestRoute = List(a, b, c, d).min
  //    if(shortestRoute < 100000000) {
  //      var nx = getx
  //      var ny = gety
  //      //finish code
  //      if(shortestRoute == a) ny-1
  //      if(shortestRoute == b) ny+1
  //      if(shortestRoute == c) nx+1
  //      if(shortestRoute == d) nx-1
  //    }
  //  }

  def mLeft = if (x - 1 >= 0 && x < level.maze.size && y >= 0 && y < level.maze(0).size && level.maze(y.toInt)(x.toInt).canPass) {
    x -= 1
  }
  def mRight = if (x + 1 >= 0 && x < level.maze.size && y >= 0 && y < level.maze(0).size && level.maze(y.toInt)(x.toInt).canPass) {
    x += 1
  }
  def mUp = if (x >= 0 && x < level.maze.size && y - 1 >= 0 && y < level.maze(0).size && level.maze(y.toInt)(x.toInt).canPass) {
    y -= 1
  }
  def mDown = if (x >= 0 && x < level.maze.size && y + 1 >= 0 && y < level.maze(0).size && level.maze(y.toInt)(x.toInt).canPass) {
    y += 1
  }

  def makePassable: PassableEntity = new PassableEntity(x, y, Entity.enemyValue)

}

